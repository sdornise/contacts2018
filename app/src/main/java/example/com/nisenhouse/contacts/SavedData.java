package example.com.nisenhouse.contacts;

/**
 * Created by dor on 28/04/18.
 */

public class SavedData {
    String spreadsheetID = null;
    String accountName = null;

    public SavedData(String spreadsheetID, String accountName) {
        this.spreadsheetID = spreadsheetID;
        this.accountName = accountName;
    }

    public String getSpreadsheetID() {
        return spreadsheetID;
    }

    public String getAccountName() {
        return accountName;
    }

    @Override
    public String toString() {
        return "spreadsheetID: " + spreadsheetID +
                ", accountName: " + accountName;
    }
}
