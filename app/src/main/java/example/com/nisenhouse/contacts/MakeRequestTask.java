package example.com.nisenhouse.contacts;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static example.com.nisenhouse.contacts.MainActivity.REQUEST_AUTHORIZATION;

/**
 * An asynchronous task that handles the Google Sheets API call.
 * Placing the API calls in their own task ensures the UI stays responsive.
 */
public class MakeRequestTask extends AsyncTask<Void, Void, List<Contact>> {
    private MainActivity mActivity;
    private static final String TAG = "CT.MakeRequestTask";
    private com.google.api.services.sheets.v4.Sheets mService = null;
    private Exception mLastError = null;
    private String mSpreadsheetId = null;

    MakeRequestTask(MainActivity activity, GoogleAccountCredential credential, String spreadsheetId) {
        this.mActivity = activity;
        this.mSpreadsheetId = spreadsheetId;
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new com.google.api.services.sheets.v4.Sheets.Builder(
                transport, jsonFactory, credential)
                .setApplicationName(mActivity.getResources().getString(R.string.app_name))
                .build();
    }

    /**
     * Background task to call Google Sheets API.
     * @param params no parameters needed for this task.
     */
    @Override
    protected List<Contact> doInBackground(Void... params) {
        if (mSpreadsheetId == null) {
            mLastError = null;
            cancel(true);
        }
        try {
            return getDataFromApi();
        } catch (UserRecoverableAuthIOException e) {
            mActivity.startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
        } catch (Exception e) {
            mLastError = e;
            cancel(true);
        }
        return mActivity.readAllContacts();
    }

    /**
     * Fetch a list of names and majors of students in a sample spreadsheet:
     * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
     * @return List of names and majors
     * @throws IOException
     */
    private List<Contact> getDataFromApi() throws IOException {
//            String spreadsheetId = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"; //example
//            String spreadsheetId = "1eN00YViQeGwkuL9vGQ7B0nP6JhvwkcY4PdgshPpvpeI"; // open copy
//        String spreadsheetId = "13c5WwdmqAcUth8qVpoFBwETIbGOm0Uap_ZvC0Jmnq7E"; // protected copy
//            String range = "Class Data!A2:E";
        String range = "Sheet1!A2:E";
        List<Contact> results = new ArrayList<>();
        ValueRange response = this.mService.spreadsheets().values()
                .get(this.mSpreadsheetId, range)
                .execute();
        List<List<Object>> values = response.getValues();
        if (values != null) {
            for (List row : values) {
                if (row.size() < 2) {
                    continue;
                }

                String name = row.get(0).toString().trim();
                String position = row.get(1).toString().trim();
                String telephone = null;
                String email = null;

                if (row.size() >= 3) {
                    telephone = row.get(2).toString().trim();
                }
                if (row.size() >= 4) {
                    email = row.get(3).toString().trim();
                }
                results.add(new Contact(
                    name,
                    position,
                    telephone,
                    email));
            }
        }

        return results;
    }


    @Override
    protected void onPreExecute() {
        mActivity.mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    protected void onPostExecute(List<Contact> output) {
        mActivity.mSwipeRefreshLayout.setRefreshing(false);
        if (mLastError != null) {
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.no_result_using_fallback), Toast.LENGTH_LONG).show();
            Log.e(TAG, "no result using fallback", mLastError);
            // using fallback! (readAllContacts)
            return;
        }

        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.data_retrieved_from_google_spreadsheet), Toast.LENGTH_LONG).show();
        Log.i(TAG, "data retrieved from google spreadsheet");
        mActivity.writeAllContacts();
        mActivity.init(output);
    }

    @Override
    protected void onCancelled() {
        mActivity.mSwipeRefreshLayout.setRefreshing(false);
        if (mLastError != null) {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                mActivity.showGooglePlayServicesAvailabilityErrorDialog(
                        ((GooglePlayServicesAvailabilityIOException) mLastError)
                                .getConnectionStatusCode());
                Log.e(TAG, "GooglePlayServicesAvailabilityIOException", mLastError);
            } else if (mLastError instanceof UserRecoverableAuthIOException) {
                Log.i(TAG, "UserRecoverableAuthIOException");
                mActivity.startActivityForResult(
                        ((UserRecoverableAuthIOException) mLastError).getIntent(),
                        REQUEST_AUTHORIZATION);
            } else {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.fetching_error, mLastError.getMessage()), Toast.LENGTH_LONG).show();
                Log.e(TAG, "fetching error", mLastError);
            }
        } else {
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.request_cancelled), Toast.LENGTH_LONG).show();
            Log.e(TAG, "request cancelled");
        }
    }
}
