package example.com.nisenhouse.contacts;

/**
 * Created by Nisenhouse on 18/03/2016.
 */
public class Contact {
    String name,position,telephone,email;

    private Contact(){};

    public Contact(String name, String position, String telephone, String email){
        this.name = name;
        this.position = position;
        this.telephone = telephone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "" + name + ", " +
                position + ", " +
                telephone + ", " +
                email;

    }
}
