package example.com.nisenhouse.contacts;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static java.lang.System.exit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, EasyPermissions.PermissionCallbacks
{

    public static final String MY_CONTACTS_JSON = "/myContacts.json";
    public static final String RESOURCE_PATH_CONF = "/resourcePath.conf";
    public static final String URI_D_PART = "/d/";

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    private static final int REQUEST_PERMISSION_GET_ALL_ACCOUNTS = 1004;
    private static final int REQUEST_CODE_OPENER = 1005;
    static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 1006;
    private static final int REQUEST_PERMISSION_GET_ALL_ACCOUNTS_O = 1007;
    private static final int REQUEST_GOOGLE_ACCOUNTS_ACCESS_CODE = 1008;
    private static final int REQUEST_CODE_SIGN_IN = 1009;

    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = { SheetsScopes.SPREADSHEETS_READONLY };
    private static final String TAG = "ConTacts";
    private static final String AUTHORITY = "example.com.nisenhouse.contacts";

    final String SCOPE="oauth2:https://www.googleapis.com/auth/userinfo.profile";

    private boolean showContacts = false;
    SwipeRefreshLayout mSwipeRefreshLayout;

    GoogleAccountCredential mCredential;
    private List<Contact> contacts;
    private String mSpreadsheetId;
    private GoogleSignInClient mGoogleSignInClient;
    private DriveClient mDriveClient;
    private int knownAccountNumber;

    /*
     * Based on:
     * https://stackoverflow.com/questions/20399371/getting-the-crash-log-and-send-it-as-email
     */
    private Thread.UncaughtExceptionHandler handleAppCrash =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    Log.e(TAG, "ConTacts has crushed!" , ex);
                    sendLoagcatMail();
                    exit(2);
                }
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(handleAppCrash);

        setContentView(R.layout.swipe_main_con_tacts);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        ImageButton mToggleAccounts = headerLayout.findViewById(R.id.toggle_accounts_button);
        mToggleAccounts.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);

        mToggleAccounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationView.getMenu().clear();
                showContacts = !showContacts;
                ImageButton mToggleAccounts = getButton();

                if (showContacts) {
                    navigationView.inflateMenu(R.menu.activity_accounts_drawer);
                    fillNavigationWithSelectableAccounts(navigationView);
                    mToggleAccounts.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
                } else {
                    navigationView.inflateMenu(R.menu.activity_main_drawer);
                    mToggleAccounts.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
                }
            }

            private ImageButton getButton() {
                final NavigationView nv = findViewById(R.id.nav_view);

                View hl = nv.getHeaderView(0);
                return (ImageButton) hl.findViewById(R.id.toggle_accounts_button);
            }
        });

        initTable();

        ensureAndroidOApprovesGetAccounts();

        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        knownAccountNumber = getAllAccounts().length;

        mSwipeRefreshLayout = findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFromResource();
            }
        });

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            System.out.println(query);
        }

        // shared link support
        Uri data = intent.getData();
        if (data != null) {
            String id = getResourceFromUri(data);
            if (id != null) {
                mSpreadsheetId = id;
                getResultsFromApi();
            }
        } else {
            refreshFromResource();
        }
    }

    private void ensureAndroidOApprovesGetAccounts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return;
        }
        final MainActivity that = this;
        new Thread(new Runnable() {
            @AfterPermissionGranted(REQUEST_PERMISSION_GET_ALL_ACCOUNTS_O)
            @Override
            public void run() {
                if (!EasyPermissions.hasPermissions(
                        that, Manifest.permission.GET_ACCOUNTS)) {
                    Log.i(TAG, "Request the GET_ACCOUNTS permission via a user dialog");
                    EasyPermissions.requestPermissions(
                            that,
                            "This app needs to access your Google account (via Contacts).",
                            REQUEST_PERMISSION_GET_ALL_ACCOUNTS_O,
                            Manifest.permission.GET_ACCOUNTS);
                    return;
                }
                try {
                    GoogleAuthUtil.requestGoogleAccountsAccess(that);
                } catch (UserRecoverableAuthException e) {
                    startActivityForResult(e.getIntent(), REQUEST_GOOGLE_ACCOUNTS_ACCESS_CODE);
                    Log.w(TAG,"using intent to get get accounts permission", e);
                } catch (IOException | GoogleAuthException e) {
                    Log.e(TAG,"failed asking for getting google accounts permission", e);
                }
            }
        }).start();
    }

    private String getResourceFromUri(Uri data) {
        Log.i(TAG, String.format("getting data from URI %s", data));
        String uri = data.toString();
        int idx = uri.indexOf(URI_D_PART);
        if (idx < 0) {
            return uri;
        }
        String str = uri.substring(idx + URI_D_PART.length());
        return str.split("/")[0];
    }

    @Override
    public boolean onSearchRequested() {
        //findViewById(R.id.sear);
        return super.onSearchRequested();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            resetTable(intent.getStringExtra(SearchManager.QUERY));
        }
    }

    private void refreshFromResource() {
        SavedData savedData = readResourceData();
        if (savedData != null) {
            Log.i(TAG, String.format("got saved data: %s", savedData.toString()));
            mSpreadsheetId = savedData.getSpreadsheetID();
            setAccount(savedData.getAccountName());
        }
        getResultsFromApi();
    }

    private void fillNavigationWithSelectableAccounts(NavigationView navigationView) {
        Log.i(TAG, "filling navigation with accounts");
        Menu menu = navigationView.getMenu();
        menu.clear();
        Account[] accounts = getAllAccounts();
        Log.i(TAG, String.format("found %d accounts", accounts.length));

        for (Account a : accounts){
            if(a.equals(mCredential.getSelectedAccount())) {
                continue;
            }
            menu.add(Menu.NONE,Menu.NONE,Menu.NONE, a.name); //TODO: set icon
        }

        menu.add(Menu.NONE,R.id.add_new_google_account,Menu.NONE, R.string.add_new_google_account);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.i(TAG, "inflating options menu");
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.i(TAG, String.format("option item was selected %d", id));

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.search_action_item:
                Log.i(TAG, "search was selected");
                onSearchRequested();
                break;
            case R.id.action_settings:
                Log.i(TAG, "settings was selected");
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void performPermissionCheck(String permission) {
        if (EasyPermissions.hasPermissions(
                this, permission)) {
            Toast.makeText(this, "ACCESS GRANTED", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "ACCESS DENIED", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Log.i(TAG, String.format("navigation item was selected %d", id));

        switch (id) {
            case R.id.nav_import:
                Log.i(TAG, "import was selected");
                openFileFromGoogleDrive();
                break;
            case R.id.add_new_google_account:
                Log.i(TAG, "add new account was selected");
                addNewGoogleAccount();
                break;
            case R.id.send_logs:
                Log.i(TAG, "sending logs to the developer");
                sendLoagcatMail();
                break;

            case R.id.permission_check_internet:
                Log.i(TAG, "permission_check_internet was selected");
                performPermissionCheck(Manifest.permission.INTERNET);
                break;
            case R.id.permission_check_network_state:
                Log.i(TAG, "permission_check_network_state was selected");
                performPermissionCheck(Manifest.permission.ACCESS_NETWORK_STATE);
                break;
            case R.id.permission_check_get_accounts:
                Log.i(TAG, "permission_check_get_accounts was selected");
                performPermissionCheck(Manifest.permission.GET_ACCOUNTS);
                break;
//            case R.id.permission_check_manage_accounts:
//                Log.i(TAG, "permission_check_manage_accounts was selected");
//                performPermissionCheck(Manifest.permission.MAN);
//                break;
            case R.id.permission_check_write_storage:
                Log.i(TAG, "permission_check_write_storage was selected");
                performPermissionCheck(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                break;

            default:
                Account account = setAccount(item.getTitle().toString());
                if (account != null) {
                    Log.i(TAG, String.format("an account was selected: %s", account.name));
                    getResultsFromApi();
                }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void addNewGoogleAccount() {
        Log.i(TAG, "adding a new account");
        Intent addAccountIntent = new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{"com.google"});
        } else {
            Log.w(TAG, "version too early for new account filtering");
        }
        this.startActivity(addAccountIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "resumed");
        int newAccountNumber = getAllAccounts().length;
        if (showContacts && knownAccountNumber != newAccountNumber) {
            knownAccountNumber = newAccountNumber;
            fillNavigationWithSelectableAccounts((NavigationView) findViewById(R.id.nav_view));
        }
    }

    private Account setAccount(String accountName) {
        Log.i(TAG, String.format("setting account by name %s", accountName));
        for (Account a : getAllAccounts()) {
            if (a.name.equals(accountName)) {
                setAccount(a);
                return a;
            }
        }
        Log.w(TAG, "did not find any account by this name");
        return null;
    }

    private void setAccount(Account a) {
        if (a == null) {
            Log.e(TAG, "tried to select a null account");
            return;
        }
        Log.i(TAG, String.format("setting account %s", a.name));

        mCredential.setSelectedAccount(a);
        if (showContacts) {
            fillNavigationWithSelectableAccounts((NavigationView) findViewById(R.id.nav_view));
        }
        new GetNameInForeground(MainActivity.this, a, SCOPE).execute();

        signOut();
        signIn(a.name);
    }

    public void setAccountDetails(String email, String displayName) {
        final NavigationView navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
//        final ImageView accountImage = headerLayout.findViewById(R.id.account_image);
//        new DownLoadImageTask(accountImage).execute(imageUri);
//        accountImage.setImageURI(Uri.parse(imageUri));
        final TextView accountNameLabel = headerLayout.findViewById(R.id.account_name_label);
        accountNameLabel.setText(displayName);
        final TextView accountEmailLabel = headerLayout.findViewById(R.id.account_email_label);
        accountEmailLabel.setText(email);
    }


    /**
     * Attempt to call the API, after verifying that all the preconditions are
     * satisfied. The preconditions are: Google Play Services installed, an
     * account was selected and the device currently has online access. If any
     * of the preconditions are not satisfied, the app will prompt the user as
     * appropriate.
     */
    private void getResultsFromApi() {
        if (! isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (! isDeviceOnline()) {
            Toast.makeText(this, getResources().getString(R.string.no_network_connection), Toast.LENGTH_LONG).show();
            Log.e(TAG, "no network connection");
            // use fallback!
            this.contacts = readAllContacts();
            this.init();
            mSwipeRefreshLayout.setRefreshing(false);
        } else if (mSpreadsheetId == null || mSpreadsheetId.isEmpty()) {
            Toast.makeText(this, getResources().getString(R.string.no_spreadsheet_was_selected), Toast.LENGTH_LONG).show();
            Log.e(TAG, "no spreadsheet was selected");
        } else {
            Log.i(TAG, "making request for data");
            new MakeRequestTask(this, mCredential, mSpreadsheetId).execute();
        }
    }

    /**
     * Attempts to set the account used with the API credentials. If an account
     * name was previously saved it will use that one; otherwise an account
     * picker dialog will be shown to the user. Note that the setting the
     * account to use with the credentials object requires the app to have the
     * GET_ACCOUNTS permission, which is requested here if it is not already
     * present. The AfterPermissionGranted annotation indicates that this
     * function will be rerun automatically whenever the GET_ACCOUNTS permission
     * is granted.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ALL_ACCOUNTS)
    private void chooseAccount() {
        Log.i(TAG, "choosing account");
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                setAccount(accountName);
//                getResultsFromApi();
            } else {
                Log.i(TAG, "Start a dialog from which the user can choose an account");
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            Log.i(TAG, "Request the GET_ACCOUNTS permission via a user dialog");
            //
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ALL_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode code indicating the result of the incoming
     *     activity result.
     * @param data Intent (containing result data) returned by incoming
     *     activity result.
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                Log.i(TAG,"google play services requested");
                if (resultCode != RESULT_OK) {
                    Toast.makeText(this, getResources().getString(R.string.common_google_play_services_unsupported_text), Toast.LENGTH_LONG).show();
                    Log.e(TAG,"google play services unsupported");
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                Log.i(TAG,"account picker services requested");
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        Log.i(TAG,String.format("got accountName %s", accountName));
//                        SharedPreferences settings =
//                                getPreferences(Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = settings.edit();
//                        editor.putString(PREF_ACCOUNT_NAME, accountName);
//                        editor.apply();
                        setAccount(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                Log.i(TAG,"authorization requested");
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
            case REQUEST_CODE_OPENER:
                Log.i(TAG,"opener requested");
                if (resultCode == RESULT_OK) {
                    DriveId mCurrentDriveId = data.getParcelableExtra(
                            OpenFileActivityOptions.EXTRA_RESPONSE_DRIVE_ID);
                    mSpreadsheetId = mCurrentDriveId.getResourceId();
                    Log.i(TAG,String.format("got spreadsheet %s", mSpreadsheetId));
                    saveResourceData(mSpreadsheetId, mCredential.getSelectedAccountName());
                    getResultsFromApi();
                }
                break;
            case REQUEST_GOOGLE_ACCOUNTS_ACCESS_CODE:
                Log.i(TAG,"requested google account access code");
                ensureAndroidOApprovesGetAccounts();
                break;
            case REQUEST_CODE_SIGN_IN:
                Log.e(TAG,"no account was selected from picker");
                if (resultCode != RESULT_OK || data == null) {
                    Log.e(TAG,"data for REQUEST_CODE_SIGN_IN was null or wrong result code");
                    Log.d(TAG, String.format("data = %s; resultCode = %s", new Gson().toJson(data), resultCode));
                    break;
                }
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount googleSignInAccount = task.getResult();
                if (googleSignInAccount == null || googleSignInAccount.getAccount() == null) {
                    Log.e(TAG,"no account was selected from picker");
                } else {
                    Account account = googleSignInAccount.getAccount();
                    Log.i(TAG,"managed to select account " + account.name);
                    setAccount(account);
                }
                break;
        }
    }

    /**
     * Respond to requests for permissions at runtime for API 23 and above.
     * @param requestCode The request code passed in
     *     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *     which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    /**
     * Callback for when a permission is granted using the EasyPermissions
     * library.
     * @param requestCode The request code associated with the requested
     *         permission
     * @param list The requested permission list. Never null.
     */
    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> list) {
        Toast.makeText(this, getResources().getString(R.string.permission_granted), Toast.LENGTH_LONG).show();
        Log.i(TAG,"permission granted");
    }

    /**
     * Callback for when a permission is denied using the EasyPermissions
     * library.
     * @param requestCode The request code associated with the requested
     *         permission
     * @param list The requested permission list. Never null.
     */
    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> list) {
        Toast.makeText(this, getResources().getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
        Log.i(TAG,"permission denied");
    }

    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            Log.e(TAG, "connMgr is null");
            return false;
        }
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     * @return true if Google Play Services is available and up to
     *     date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        Log.d(TAG,"isGooglePlayServicesAvailable");
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        Log.d(TAG,"acquireGooglePlayServices");
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        Log.d(TAG,"showGooglePlayServicesAvailabilityErrorDialog");
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                MainActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    //++++++++ MANAGE TABLE +++++++++++//
    public void init() {
        init(this.contacts);
    }

    public void init(List<Contact> contacts) {
        if (contacts == null)
            return;
        this.contacts = contacts;

        resetTable();
    }

    private void resetTable() {
        resetTable("");
    }

    private void resetTable(String filter) {
        Log.i(TAG, "resetting table");
        TableLayout ll = findViewById(R.id.displayLinear);
        ll.removeAllViews();

        if (this.contacts == null) {
            return;
        }
        addHeader(ll);

        int pos = 1;
        for (Contact c : this.contacts) {
            if (
                    (c.getName() != null && c.getName().contains(filter)) ||
                            (c.getPosition() != null && c.getPosition().contains(filter)) ||
                            (c.getEmail() != null && c.getEmail().contains(filter)) ||
                            (c.getTelephone() != null && c.getTelephone().contains(filter)))
                addRow(ll, pos++, c.getName(), c.getPosition(), c.getTelephone(), c.getEmail());
        }
    }

    private void initTable() {
        TableLayout ll = findViewById(R.id.displayLinear);
        ll.setColumnStretchable(0, true);
        ll.setColumnStretchable(1, true);
        ll.setColumnStretchable(2, true);
    }

    private void addHeader(TableLayout ll) {
        TableRow row = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);
        TextView txtName = new TextView(this);
        txtName.setText(getResources().getText(R.string.name));
        row.addView(txtName);
        TextView txtPos = new TextView(this);
        txtPos.setText(getResources().getText(R.string.position));
        row.addView(txtPos);
        TextView txtTel = new TextView(this);
        txtTel.setText(getResources().getText(R.string.telephone));
        row.addView(txtTel);
        ll.addView(row, 0);
    }

    private void addRow(TableLayout ll, int idx, final String name, final String position, final String telephone, final String email) {

        final MainActivity thisActivity = this;

        TableRow row = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);
        TextView txtName = new TextView(this);
        txtName.setText(name);
        row.addView(txtName);
        TextView txtPos = new TextView(this);
        txtPos.setText(position);
        if (email != null && !email.isEmpty()) {
            txtPos.setTextColor(android.graphics.Color.parseColor("#aa0000"));
        } else {
            txtPos.setTextColor(android.graphics.Color.parseColor("#909090"));
        }
        final MainActivity that = this;
        txtPos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (email == null || email.isEmpty()) {
                    Toast.makeText(that, getResources().getString(R.string.no_email_specified), Toast.LENGTH_LONG).show();
                    Log.i(TAG, "no email was specified");
                    return;
                }

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
//                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//                i.putExtra(Intent.EXTRA_TEXT   , "body of email");

                filterGmail(emailIntent);
                try {
                    startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_email_by, position)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Log.e(TAG, "no email apps", ex);
                    Toast.makeText(thisActivity, getResources().getString(R.string.no_email_apps), Toast.LENGTH_SHORT).show();
                }
            }
        });

        row.addView(txtPos);
        TextView txtTel = new TextView(this);
        txtTel.setText(telephone);
        txtTel.setTextColor(android.graphics.Color.parseColor("#0000aa"));

        txtTel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + telephone));
                try {
                    startActivity(
                            Intent.createChooser(
                                    phoneIntent,
                                    getResources().getString(R.string.call_by, name, position)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(thisActivity, getResources().getString(R.string.no_telephone_apps_found), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "no telephone_apps found", ex);
                }
            }
        });

        TableLayout.LayoutParams tableRowParams =
                new TableLayout.LayoutParams
                        (TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);

        tableRowParams.setMargins(10, 10, 10, 10);
        row.setLayoutParams(tableRowParams);

        row.addView(txtTel);

        ll.addView(row, idx);
    }

    /*
     * Based on:
     * https://stackoverflow.com/questions/9730243/how-to-filter-specific-apps-for-action-send-intent-and-set-a-different-text-for/18068122#18068122
     */
    private void filterGmail(Intent emailIntent) {
        PackageManager pm = getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        for (ResolveInfo ri : resInfo) {
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("com.google.android.gm")) {
                emailIntent.setPackage(packageName);
                return;
            }
        }
    }

    // ----------------- END -----------------//

    // +++++++++++++++++ SAVING DATA +++++++++//

    public void writeAllContacts() {
        writeAllContacts(new File(getAppDir() + MY_CONTACTS_JSON));
    }

    private void writeAllContacts(File file) {
        Log.i(TAG, "write all contacts");
        if (this.contacts == null)
            return;

        try {
            Log.d("ConTacts", file.getAbsolutePath());
            if (!file.exists()) {
                Log.d("Contacts", "creating");
                if (!file.createNewFile()) {
                    throw new IOException("File wasn't created");
                }
            }
            byte[] data = new Gson().toJson(contacts).getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "exception occurred while saving contacts", e);
        }
    }

    public List<Contact> readAllContacts() {
        File file = new File(getAppDir() + MY_CONTACTS_JSON);
        return readAllContacts(file);
    }

    private List<Contact> readAllContacts(File file) {
        Log.i(TAG, "read all contacts");
        String str;

        try {
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            str = new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "exception while reading all contacts", e);
            return null;
        }

        return new Gson().fromJson(str, new TypeToken<List<Contact>>(){}.getType());
    }

    private void saveResourceData(String spreadsheetId, String accountName) {
        SavedData savedData = new SavedData(spreadsheetId, accountName);
        saveResourceData(new File(getAppDir() + RESOURCE_PATH_CONF), savedData);
    }

    private void saveResourceData(File file, SavedData savedData) {
        Log.i(TAG, "saving resource data");
        try {
            Log.d("Contacts", file.getAbsolutePath());
            if (!file.exists()) {
                Log.d("Contacts", "creating");
                if(!file.createNewFile()) {
                    throw new IOException("file couldn't be created");
                }
            }
            byte[] data = new Gson().toJson(savedData).getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            Log.e(TAG,"exception while saving resource data", e);
        }
    }

    private SavedData readResourceData() {
        File file = new File(getAppDir() + RESOURCE_PATH_CONF);
        try {
            return new Gson().fromJson(readResourceData(file), SavedData.class);
        } catch (Exception e) {
            Log.e(TAG, "error reading resource data", e);
            return null;
        }
    }

    private String readResourceData(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            return new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "error reading resource data", e);
            return null;
        }
    }

    private String getAppDir() {
        PackageManager m = getPackageManager();
        String $ = null;
        try {
            PackageInfo p = m.getPackageInfo(getPackageName(), 0);
            $ = p.applicationInfo.dataDir;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, "Error Package name not found ", e);
        }
        return $;
    }

    // ----------------- END -----------------//

    // ++++++++++ SPREADSHEET PICKER +++++++++//
    /**
     *  Open list of folder and file of the Google Drive
     */
    public void openFileFromGoogleDrive() {
        Log.i(TAG, "opening file from google drive");
        if (mCredential.getSelectedAccount() == null) {
            Toast.makeText(this, getResources().getString(R.string.no_account_was_selected), Toast.LENGTH_LONG).show();
            Log.e(TAG, "no account was selected");
            return;
        }

        // Build activity options.
        final OpenFileActivityOptions openFileActivityOptions =
                new OpenFileActivityOptions.Builder()
                        .setMimeType(Collections.singletonList("application/vnd.google-apps.spreadsheet"))
                        .build();

        // Start a OpenFileActivityIntent
        mDriveClient.newOpenFileActivityIntentSender(openFileActivityOptions)
                .addOnSuccessListener(new OnSuccessListener<IntentSender>() {
                    @Override
                    public void onSuccess(IntentSender intentSender) {
                        try {
                            startIntentSenderForResult(
                                    intentSender,
                                    REQUEST_CODE_OPENER,
                            /* fillInIntent= */ null,
                            /* flagsMask= */ 0,
                            /* flagsValues= */ 0,
                            /* extraFlags= */ 0);
                        } catch (IntentSender.SendIntentException e) {
                            Log.w(TAG, "Unable to send intent.", e);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Unable to create OpenFileActivityIntent.", e);
            }
        });
    }

    // ----------------- END -----------------//

    // ++++++++++ GOOGLE SIGN-IN +++++++++++++//

    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private Account[] getAllAccounts() {
        ensureAndroidOApprovesGetAccounts();
        if (!EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            Log.i(TAG, "Request the GET_ACCOUNTS permission via a user dialog");
            //
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your accounts.",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
            return new Account[0];
        }

//        AccountManager accountManager = AccountManager.get(this);
//        return accountManager.getAccountsByType("com.google");
        return mCredential.getAllAccounts();
    }

    private void signOut() {
        Log.i(TAG, "sign out");
        if (isSignedIn()) {
            mGoogleSignInClient.signOut();
        }
    }

    public boolean isSignedIn() {
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);
        return mGoogleSignInClient != null
                && (signInAccount != null
                && signInAccount.getGrantedScopes().contains(Drive.SCOPE_FILE));
    }

    private void signIn(String accountName) {
        Log.i(TAG, "Start sign-in.");
        mGoogleSignInClient = getGoogleSignInClient(accountName);

        final MainActivity that = this;
        // Attempt silent sign-in
        mGoogleSignInClient.silentSignIn()
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        if (googleSignInAccount.getAccount() == null) {
                            Toast.makeText(that, getResources().getString(R.string.account_was_not_connected), Toast.LENGTH_LONG).show();
                            Log.w(TAG, "account was not connected");
                            return;
                        }

                        createDriveClients(googleSignInAccount);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Silent sign-in failed, display account selection prompt
                Log.w(TAG, "Silent sign-in failed, display account selection prompt");
                startActivityForResult(
                        mGoogleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
            }
        });
    }

    /**
     * Builds a Google sign-in client.
     */
    private GoogleSignInClient getGoogleSignInClient(String accountName) {
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Drive.SCOPE_FILE)
                        .requestEmail()
                        .requestProfile()
                        .setAccountName(accountName)
                        .build();
        return GoogleSignIn.getClient(this, signInOptions);
    }

    private void createDriveClients(GoogleSignInAccount googleSignInAccount) {
        Log.i(TAG, "Update view with sign-in account.");
        // Build a drive client.
        mDriveClient = Drive.getDriveClient(getApplicationContext(), googleSignInAccount);
        // Build a drive resource client.
//        mDriveResourceClient =
//                Drive.getDriveResourceClient(getApplicationContext(), googleSignInAccount);
    }

    // ----------------- END -----------------//

    @AfterPermissionGranted(REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE)
    public void sendLoagcatMail(){
        if (!EasyPermissions.hasPermissions(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.i(TAG, "Request the WRITE_EXTERNAL_STORAGE permission via a user dialog");
            //
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your external storage for saving logs.",
                    REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return;
        }

        // save logcat in file
        File outputFile = new File(Environment.getExternalStorageDirectory(),
                "logcat.txt");
        if (outputFile.exists()) {
            outputFile.delete();
        }

        try {
//                outputFile.createNewFile();
            Runtime.getRuntime().exec(
                    "logcat -s " + TAG + " -f " + outputFile.getAbsolutePath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //send file using email
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // Set type to "email"
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {getResources().getText(R.string.developer_email).toString()};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        // the attachment=
        emailIntent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(this, AUTHORITY, outputFile));
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ConTacts logs");
        emailIntent.setType("message/rfc822");
        filterGmail(emailIntent);
        startActivity(Intent.createChooser(emailIntent , "Send email..."));
    }


}
