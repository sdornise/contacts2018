package example.com.nisenhouse.contacts;

import android.accounts.Account;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * https://sites.google.com/site/lokeshurl/android-gmail-integration-getting-user-photo-and-user-name
 */
public class GetNameInForeground extends AbstractGetNameTask {


    GetNameInForeground(MainActivity activity, Account account, String mScope) {
        super(activity, account, mScope);
    }

    /**
     37
     * Get a authentication token if one is not available. If the error is not recoverable then
     38
     * it displays the error message on parent activity right away.
     39
     */
    @Override
    protected String fetchToken() throws IOException {
        try {
            return GoogleAuthUtil.getToken(mActivity, mAccount, mScope);
        } catch (GooglePlayServicesAvailabilityException playEx) {
            // GooglePlayServices.apk is either old, disabled, or not present.
        } catch (UserRecoverableAuthException userRecoverableException) {
            // Unable to authenticate, but the user can fix this.
            // Forward the user to the appropriate activity.
            mActivity.startActivityForResult(userRecoverableException.getIntent(), mRequestCode);
        } catch (GoogleAuthException fatalException) {
            onError("Unrecoverable error " + fatalException.getMessage(), fatalException);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        try {
            JSONObject jsonObject = new JSONObject(AbstractGetNameTask.GOOGLE_USER_DATA);
            mActivity.setAccountDetails(
                    mAccount.name, // email
                    jsonObject.getString("name"));
//                    ,jsonObject.getString("picture"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
